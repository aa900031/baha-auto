/// <reference path="./shims-surge.d.ts" />

import { createLogin, createCheckIn } from '@baha-auto/core'
import { createRequest } from './utils/request'
import { getUsername, getPassword } from './utils/env'
import { createGuildCheckIn } from 'packages/core/src/guild-check-in'
import { createAnimeAnswer } from 'packages/core/src/anime-answer'

const bootstrap = async () => {
  const request = createRequest()
  const login = createLogin({ request })
  const checkIn = createCheckIn({ request })
  const guildCheckIn = createGuildCheckIn({ request })
  const animeAnswer = createAnimeAnswer({ request })

  try {
    const resultLogin = await login.exec({
      username: getUsername(),
      password: getPassword(),
    })

    ;(async () => {
      try {
        const result = await checkIn.exec(resultLogin)
        $notification.post('巴哈姆特', '每日簽到', `已經簽到 ${result.days} 天`)
      } catch (error) {
        const message = error instanceof Error ? error.message : error
        $notification.post('巴哈姆特', '每日簽到', `失敗：${message}`)
      }
    })()

    // ;(async () => {
    //   try {
    //     const result = await guildCheckIn.exec(resultLogin)
    //     $notification.post('巴哈姆特', '公會簽到', `成功：${result.summary.success}, 失敗：${result.summary.fail}`)
    //   } catch (error) {
    //     const message = error instanceof Error ? error.message : error
    //     $notification.post('巴哈姆特', '公會簽到', `失敗：${message}`)
    //   }
    // })()

    ;(async () => {
      try {
        const result = await animeAnswer.exec(resultLogin)
        $notification.post('巴哈姆特', '動漫通', `成功！${result.message}`)
      } catch (error) {
        const message = error instanceof Error ? error.message : error
        $notification.post('巴哈姆特', '動漫通', `失敗：${message}`)
      }
    })()

  } catch (error) {
    const message = error instanceof Error ? error.message : error
    $notification.post('巴哈姆特', '發生錯誤', `${message}`)
  }

  $done()
}

bootstrap()
