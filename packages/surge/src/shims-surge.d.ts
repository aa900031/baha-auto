declare const $persistentStore: {
  write: (data: string | null | undefined, key: string) => boolean
  read: (key: string) => string | null
}

interface IHttpClientOptions {
  url: string;
  headers?: Record<string, string>;
  body?: string;
}

interface IHttpClientCallback {
  (error: any | undefined, response: { status: number; headers: Record<string, string> }, data: string): void
}

declare const $httpClient: {
  get: (opts: IHttpClientOptions, callback: IHttpClientCallback) => void
  post: (opts: IHttpClientOptions, callback: IHttpClientCallback) => void
}

declare const $notification: {
  post: (title: string, subtitle: string, body: string) => void
}

declare const $done: (value?: any) => void

declare const $request: {
  url: string;
  method: string;
  headers: Record<string, string>;
}
