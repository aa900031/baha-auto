/// <reference path="../shims-global.d.ts" />

export const getUsername = (): string => {
  const result = BAHA_USERNAME
  if (!result) throw new Error('請設定 BAHA_USERNAME')

  return result
}

export const getPassword = (): string => {
  const result = BAHA_PASSWORD
  if (!result) throw new Error('請設定 BAHA_PASSWORD')

  return result
}
