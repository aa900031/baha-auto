/// <reference path="../shims-surge.d.ts" />

import type { IRequest } from '@baha-auto/core'

const getContentType = (
  data: Parameters<IRequest>[0],
) => {
  const type = data.headers?.['Content-Type'] ?? 'application/x-www-form-urlencoded'
  const splited = type.split(';').map(s => s.trim())
  return splited[0]
}

const resolveMethod = (
  data: Parameters<IRequest>[0],
) => {
  switch (data.method) {
    case 'GET':
      return 'get'
    case 'POST':
      return 'post'
  }
}

const stringifyUrlEncodedObject = (
  data: Record<string, any>
) => Object
  .keys(data)
  .filter(key => data[key] !== null && typeof data[key] !== 'undefined')
  .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
  .join('&')

const resolveBody = (
  data: Parameters<IRequest>[0],
): string | undefined => {
  if (data.method === 'GET') return
  if (!data.body) return

  const type = getContentType(data)

  switch (type) {
    case 'application/x-www-form-urlencoded':
      return stringifyUrlEncodedObject(data.body)
    case 'application/json':
      return JSON.stringify(data.body)
    default:
      throw new Error(`不支援的 Content-Type: ${type}`)
  }
}

export const createRequest = (): IRequest => {
  return (config) => new Promise((resolve, reject) => {
    $httpClient[resolveMethod(config)]({
      url: config.url,
      headers: config.headers,
      body: resolveBody(config),
    }, (error, response, data) => {
      if (error) {
        reject({
          error,
        })
      } else if (response.status !== 200) {
        reject({
          ...response,
          body: data,
        })
      } else {
        resolve({
          ...response,
          body: data,
        })
      }
    })
  })
}
