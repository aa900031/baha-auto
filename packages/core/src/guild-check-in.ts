import { Awaited } from 'ts-essentials'
import type { IRequest } from './request'

export interface IGuildCheckInProps {
  request: IRequest
}

export interface IGuildCheckIn {
  exec: (data: {
    BAHARUNE: string
  }) => Promise<{
    summary: {
      success: number
      fail: number
    }
    list: {
      name: string
      success: boolean
    }[]
  }>
}

export const createGuildCheckIn = ({
  request,
}: IGuildCheckInProps): IGuildCheckIn => {
  type Self = IGuildCheckIn

  const fetchList = async (data: {
    rune: string
  }): Promise<{
    data: {
      manageList: {
        sn: string
        name: string
      }[]
      joinList: {
        sn: string
        name: string
      }[]
    }
  }> => {
    const { body } = await request({
      method: 'GET',
      url: 'https://api.gamer.com.tw/guild/v1/guild_my.php',
      headers: {
        Cookie: `BAHARUNE=${data.rune}`
      },
    })

    return JSON.parse(body)
  }

  const submitCheckIn = async (data: {
    sn: string
    rune: string
  }): Promise<{
    ok: boolean
    msg: string
  }> => {
    const { body } = await request({
      method: 'POST',
      url: 'https://guild.gamer.com.tw/ajax/guildSign.php',
      headers: {
        Cookie: `BAHARUNE=${data.rune}`
      },
      body: {
        sn: data.sn,
      },
    })

    return JSON.parse(body)
  }

  const compressGuildList = (
    data: Awaited<ReturnType<typeof fetchList>>,
  ) => {
    return [
      ...data.data.joinList,
      ...data.data.manageList,
    ]
  }

  const exec: Self['exec'] = async ({
    BAHARUNE,
  }) => {
    const guilds = compressGuildList(await fetchList({ rune: BAHARUNE }))
    const results = await Promise.allSettled(guilds.map((guild) => submitCheckIn({ rune: BAHARUNE, sn: guild.sn })))

    const list = results.map((result, index) => {
      switch (result.status) {
        case 'fulfilled':
          return {
            name: guilds[index].name,
            success: true,
          }

        case 'rejected':
          return {
            name: guilds[index].name,
            success: false,
          }
      }
    }).filter(Boolean)

    return {
      summary: {
        success: list.filter((item) => item.success).length,
        fail: list.filter(item => !item.success).length,
      },
      list,
    }
  }

  return {
    exec,
  }
}
