import { IRequest } from './request'

export interface ILoginProps {
  request: IRequest
}

export interface ILogin {
  exec: (data: {
    username: string,
    password: string,
    totp?: string,
  }) => Promise<{
    BAHARUNE: string
  }>
}

export const createLogin = ({
  request,
}: ILoginProps): ILogin => {
  type Self = ILogin
  const exec: Self['exec'] = async ({
    username,
    password,
    totp,
  }) => {
    const code = "6666"

    const response = await request({
      method: 'POST',
      url: 'https://api.gamer.com.tw/mobile_app/user/v3/do_login.php',
      headers: {
        Cookie: `ckAPP_VCODE=${code}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        uid: username,
        passwd: password,
        vcode: code,
        twoStepAuth: totp,
      },
    })

    const data: {
      userid: string
    } | {
      error: {
        message: string
      }
    } = JSON.parse(response.body)

    if ('userid' in data && data.userid) {
      return {
        BAHARUNE: response.headers['Set-Cookie'].split(/BAHARUNE=(\w+)/)[1],
      }
    } else {
      const message = ('error' in data ? data.error.message : null) ?? '未知失敗'
      throw new Error(`登入：${message}`)
    }
  }

  return {
    exec,
  }
}
