import { format } from 'date-fns'
import type { Awaited } from 'ts-essentials'
import type { IRequest } from './request'

export interface IAnimeAnswerProps {
  request: IRequest
}

export interface IAnimeAnswer {
  exec: (data: {
    BAHARUNE: string
  }) => Promise<{
    success: boolean
    message: string
  }>
}

export const createAnimeAnswer = ({
  request,
}: IAnimeAnswerProps): IAnimeAnswer => {
  type Self = IAnimeAnswer

  const fetchQuestion = async (data: {
    rune: string
    date: number
  }) => {
    const { body } = await request({
      method: 'GET',
      url: `https://ani.gamer.com.tw/ajax/animeGetQuestion.php?t=${data.date}`,
      headers: {
        Cookie: `BAHARUNE=${data.rune}`
      },
    })

    return JSON.parse(body) as {
      token: string
      game: string
      question: string
      a1: string
      a2: string
      a3: string
      a4: string
    }
  }

  const fetchAnswerArticles = async () => {
    const { body } = await request({
      method: 'GET',
      url: 'https://api.gamer.com.tw/mobile_app/bahamut/v1/home.php?owner=blackXblue&page=1',
    })

    return JSON.parse(body) as {
      creation: {
        sn: string
        title: string
      }[]
    }
  }

  const findRightArticle = (
    data: Awaited<ReturnType<typeof fetchAnswerArticles>>,
    date: number,
  ) => {
    const dateText = format(date, 'MM/dd')
    return data.creation.find(c => c.title.includes(dateText))
  }

  const fetchArticleDetail = async (data: {
    sn: string
  }) => {
    const { body } = await request({
      method: 'GET',
      url: `https://api.gamer.com.tw/mobile_app/bahamut/v1/home_creation_detail.php?sn=${data.sn}`,
    })

    return JSON.parse(body) as {
      content: string
    }
  }

  const scrapeAnswer = (content: string) => {
    return content.split(/A:(\d)/)[1]
  }

  const submitAnswer = async (
    data: {
      rune: string
      answer: string
      token: string
      date: number
    }
  ) => {
    const { body } =  await request({
      method: 'POST',
      url: 'https://ani.gamer.com.tw/ajax/animeAnsQuestion.php',
      headers: {
        Cookie: `BAHARUNE=${data.rune}`
      },
			body: {
        token: data.token,
        ans: data.answer,
        date: data.date,
      },
    })

    return JSON.parse(body) as {
      ok: 1
      gift: string
    } | {
      error: 1
      msg: string
    }
  }

  const exec: Self['exec'] = async ({
    BAHARUNE,
  }) => {
    const date = Date.now()

    const question = await fetchQuestion({ rune: BAHARUNE, date })

    const articles = await fetchAnswerArticles()

    const article = findRightArticle(articles, date)
    if (!article) throw new Error('沒有查到文章')

    const detail = await fetchArticleDetail({ sn: article.sn })

    const answer = scrapeAnswer(detail.content)

    const result = await submitAnswer({ rune: BAHARUNE, token: question.token, answer, date })
    if ('error' in result) throw new Error(result.msg ?? '未知錯誤')

    return {
      success: !!result.ok,
      message: result.gift,
    }
  }

  return {
    exec,
  }
}
