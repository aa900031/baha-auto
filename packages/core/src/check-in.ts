import type { IRequest } from './request'

export interface ICheckInProps {
  request: IRequest
}

export interface ICheckIn {
  exec: (data: {
    BAHARUNE: string
  }) => Promise<{
    days: number
  }>
}

export const createCheckIn = ({
  request,
}: ICheckInProps): ICheckIn => {
  type Self = ICheckIn

  const exec: Self['exec'] = async ({
    BAHARUNE,
  }) => {
    const headers = {
      Cookie: `BAHARUNE=${BAHARUNE}`
    }

    const { body: csrfToken } = await request({
      method: 'GET',
      url: 'https://www.gamer.com.tw/ajax/get_csrf_token.php',
      headers,
    })

    const { body: bodySigin } = await request({
      method: 'POST',
      url: 'https://www.gamer.com.tw/ajax/signin.php',
      headers,
      body: {
        action: '1',
        token: csrfToken,
      },
    })

    const data: {
      data: {
        days: number
      }
    } | {
      error: {
        message: string
      }
    } = JSON.parse(bodySigin)

    if ('data' in data) {
      return {
        days: data.data.days,
      }
    } else {
      const message = ('error' in data ? data.error.message : null) ?? '未知失敗'
      throw new Error(`簽到：${message}`)
    }
  }

  return {
    exec,
  }
}
