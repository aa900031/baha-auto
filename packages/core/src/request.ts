export interface IRequest {
  (props: {
    url: string
    method: 'GET' | 'POST'
    headers?: Record<string, string>
    body?: any
  }): Promise<{
    status: number
    headers: Record<string, string>
    body: string
  }>
}
