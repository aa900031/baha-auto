import typescript from 'rollup-plugin-typescript2'
import commonjs from '@rollup/plugin-commonjs'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import { terser } from 'rollup-plugin-terser'
import { RollupOptions } from 'rollup'

const plugins: RollupOptions['plugins'] = [
  nodeResolve(),
  commonjs(),
  typescript({
    tsconfigOverride: {
      compilerOptions: {
        declaration: false,
      },
    },
  }),
]

const configs: RollupOptions[] = [
  {
    input: 'packages/surge/src/main.ts',
    output: [
      {
        file: 'packages/surge/dist/main.js',
        format: 'cjs',
        plugins: [
          terser({
            format: {
              comments: false,
            },
            keep_classnames: true,
          }),
        ]
      },
    ],
    plugins,
  },
]

export default configs
